# syntax=docker/dockerfile:1

FROM python:3.8-slim-buster

WORKDIR /app

RUN update-ca-certificates

RUN apt-get update \
    && apt-get install -y firefox-esr \
    && apt-get install -y xvfb 

COPY requirements /app/requirements

RUN --mount=type=ssh --mount=type=cache,target=/root/.cache/pip pip install -r requirements \
    --trusted-host pypi.org \ 
    --trusted-host pypi.python.org \
    --trusted-host=files.pythonhosted.org

COPY . .

CMD [ "python3", "-m", "src.main"]