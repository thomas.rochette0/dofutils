import asyncio

from src.utils.logging import logging
from .Dofutils import Dofutils
from .modules import load_cogs


async def main():
    bot = Dofutils()
    await load_cogs(bot)

    await bot.start()

asyncio.run(main(), debug=True)
