import time
import logging
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from .main import FirefoxDriver


def players_from_guild(url):
    """
    Extract a list of player from a guild URL
    """
    print('Start download')
    start = time.time()
    driver = FirefoxDriver()
    print('Get Url')
    driver.get(url)
    WebDriverWait(driver, 400).until(
        EC.presence_of_element_located((By.CLASS_NAME, "ak-accept"))
    )
    accept_button = driver.find_elements(By.CLASS_NAME, "ak-accept")
    driver.execute_script(
        "arguments[0].click();", accept_button[0]
    )  # Use javascript because button sometimes is unaccessible

    print('Find links')
    links = driver.find_elements(By.XPATH, "//*[@href]")
    players = {}
    for link in links:
        href = link.get_attribute("href")
        if "pages-persos/" in href:
            data = href.split("/")[-1]
            name = "-".join(data.split("-")[1:])
            _id = data.split("-")[0]
            players[name] = {"id": _id}

    print(f"Got players from guild url in {time.time() - start}")
    return players


def extract_jobs_from_player(_id, pseudo) -> dict:
    """
    Extract job data from a player profile
    """
    url = f"https://www.dofus.com/fr/mmorpg/communaute/annuaires/pages-persos/{_id}-{pseudo}"

    driver = FirefoxDriver()
    try:
        driver.get(url)
        WebDriverWait(driver, 20).until(
            EC.presence_of_element_located((By.CLASS_NAME, "ak-infos-jobs"))
        )
        driver.execute_script("window.stop();")
        jobs = {}
        text = ""
        for i in range(3):
            content = driver.find_elements(By.CLASS_NAME, "ak-infos-jobs")
            if len(content) == 0:
                break
            new_data = content[0].find_elements(By.CLASS_NAME, "ak-page")
            for element in new_data:
                driver.execute_script(
                    f"arguments[0].style.display = 'block';", element)
                text += element.text
            jobs = {}
        for job_data in text.split(")"):
            if job_data != "":
                job_data = job_data.lstrip().split("(")
                jobs[job_data[0].strip()] = job_data[1].split(" ")[1]
        return jobs
    except TimeoutException:
        logging.error(f"Url {url} is not OK")


def update_all_players_jobs(players: dict) -> dict:
    start = time.time()
    for pseudo, data in players.items():
        data["job"] = extract_jobs_from_player(data["id"], pseudo)

    print(f'updated in {time.time() - start}')
    return players


if __name__ == "__main__":
    guildURL = "https://www.dofus.com/fr/mmorpg/communaute/annuaires/pages-guildes/4349600222-quetsches"

    playersDict = players_from_guild(guildURL)

    for pseudo, data in playersDict.items():
        data["job"] = extract_jobs_from_player(data["id"], pseudo)

    print(playersDict)
