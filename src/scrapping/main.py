import sys
from selenium import webdriver
from selenium.webdriver.firefox.options import Options as OptionsFirefox
from selenium.webdriver.chrome.options import Options as OptionsChrome
from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.firefox.service import Service as fireService
from selenium.webdriver.chrome.service import Service as chromeService


class FirefoxDriver:

    __instance = None

    def __new__(cls):
        if cls.__instance is None:
            if sys.platform == "linux":
                from pyvirtualdisplay import Display

                display = Display(visible=0, size=(1920, 1080))
                display.start()
                path = GeckoDriverManager().install()

                options = OptionsFirefox()
                options.add_argument("--window-size=1920,1080")
                options.add_argument("--ignore-certificate-errors")
                options.add_argument(
                    "--disable-blink-features=AutomationControlled")
                options.set_capability("pageLoadStrategy", "eager")

                # Create driver
                cls.__instance = webdriver.Firefox(
                    service=fireService(executable_path=path),
                    options=options,
                )
            if 'win' in sys.platform:
                path = ChromeDriverManager().install()

                options = OptionsChrome()
                options.add_argument("--window-size=1920,1080")
                options.add_argument("--ignore-certificate-errors")
                options.add_argument(
                    "--disable-blink-features=AutomationControlled")
                options.set_capability("pageLoadStrategy", "none")

                cls.__instance = webdriver.Chrome(
                    service=chromeService(executable_path=path),
                    options=options,
                )

        return cls.__instance
