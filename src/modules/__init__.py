from src.modules.metier import cog as metier_cog
from src.modules.players import cog as players_cog

from discord.ext.commands import Bot


async def load_cogs(bot: Bot) -> None:
    await bot.load_extension(metier_cog.__name__)
    await bot.load_extension(players_cog.__name__)
