from discord.ext import commands
from src.utils.embed_messages import create_embed
from src.utils.logging import logging

class Metier(commands.Cog, name='Metier'):
    """Commandes liés aux métiers"""

    def __init__(self, bot):
        self.bot = bot


    @commands.command(pass_context=True)
    async def best(self, ctx, job_name):
        """!best <métier> (permet de connaitre le meilleur dans un métier, all permet de lister tous les métiers)"""
        p = self.bot.players
        p_list = list(p.keys())
        job_list = list(p[p_list[0]]["job"])
        if job_name != 'all' and job_name.capitalize() not in job_list:
            await ctx.send(f"{job_name.capitalize()} n'est pas dans {job_list} ou 'all' ")
        else:
            # Search jobs
            best = {}
            for job in job_list:
                best[job] = {'level': 0, 'player': None}
                for key, _dict in p.items():
                    if _dict["job"] is not None:
                        # If a player has not been connected for 6 month
                        if job in _dict["job"]:
                            # If the player started the job
                            if int(_dict["job"][job]) > best[job]['level']:
                                best[job]['level'] = int(_dict["job"][job])
                                best[job]['player'] = key
            logging(best)
            if job_name == 'all':
                embed = create_embed(f'Métier {job_name.capitalize()}', desc='Liste du joueur avec le plus haut niveau pour chaque métier', field=list(best.keys()), values=[f"{meilleur['player']} : niveau {meilleur['level']}" for meilleur in best.values()], inline=False)
                await ctx.send(embed=embed)
            else:
                embed = create_embed(f'Métier {job_name.capitalize()}', desc=f'Liste du joueur avec le plus haut niveau de {job_name.capitalize()}', field=[job_name.capitalize()], values=[f"{best[job_name.capitalize()]['player']} : niveau {best[job_name.capitalize()]['level']}"], inline=False)
                await ctx.send(embed=embed)

    @commands.command(pass_context=True)
    async def metier(self, ctx, arg):
        """!metier <métier/joueur> (permet de connaitre le niveau de tous le monde dans un métier, all permet de lister tous les métiers)"""
        p = self.bot.players
        p_list = list(p.keys())
        job_list = list(p[p_list[0]]["job"])

        if arg == 'all':
            embed = create_embed(f'Métier {arg.capitalize()}', desc='Liste du niveau des joureurs dans chaque métier', field=job_list, values=[", ".join([f"{joueur}: niveau {_dict['job'].get(job ,0)}" for joueur, _dict in p.items()]) for job in job_list], inline=False)
            await ctx.send(embed=embed)
        elif arg.capitalize() in job_list:
            embed = create_embed(f'Métier {arg.capitalize()}', desc=f'Liste du niveau des joureurs en {arg.capitalize()}', field=[arg.capitalize()], values=[", ".join([f"{joueur}: niveau {_dict['job'].get(arg.capitalize() ,0)}" for joueur, _dict in p.items()])], inline=False)
            await ctx.send(embed=embed)
        elif arg in p_list:
            embed = create_embed(f'Métier {arg.capitalize()}', desc=f'Liste du niveau des {arg.capitalize()} dans chaque métier', field=[metier for metier in p[arg]['job'].keys()], values=[f"Niveau {level}" for level in p[arg]['job'].values()], inline=False)
            await ctx.send(embed=embed)
        else:
            await ctx.send(f"{arg} n'est pas dans {job_list} ou 'all' et n'est pas dans {p_list}")

async def setup(bot):
    await bot.add_cog(Metier(bot))
