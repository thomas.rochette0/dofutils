import threading
from discord.ext import commands, tasks

from src.scrapping.players import update_all_players_jobs, players_from_guild
from src.utils.embed_messages import create_embed
from src.utils.logging import logging


class Players(commands.Cog, name='Players'):
    """Commandes liés aux joueurs"""

    def __init__(self, bot):
        self.bot = bot
        self.message_not_sent = []

        # Start tasks #########################################################
        self.send_message.start()
        self.update.start()
        self.update_guild.start()

    # TASKS ###################################################################
    @tasks.loop(seconds=10.0)
    async def send_message(self):
        """Empty the message not send buffer"""
        while self.message_not_sent:
            msg = self.message_not_sent.pop(0)
            await msg['ctx'].send(msg['message'])

    @tasks.loop(minutes=10)
    async def update(self):
        print('Started update')
        thread = ThreadMeThat(None,
                              update_all_players_jobs,
                              {"url": self.bot.guild['url']},
                              self.call_back_update,
                              sendMsg=False)
        thread.start()

    @tasks.loop(hours=10)
    async def update_guild(self):
        print('Started update Guild')
        thread = ThreadMeThat(None,
                              players_from_guild,
                              {"players": self.bot.players},
                              self.call_back_guild_url,
                              sendMsg=False)
        thread.start()

    # COMMANDS ################################################################
    @commands.command(pass_context=True)
    async def list(self, ctx):
        """!list (permet de lister les joueurs de dofus)"""
        embed = create_embed("Liste des Joueurs", field=list(self.bot.players.keys()), values=[
                             x.get('owner', '?') for x in self.bot.players.values()])
        logging(dir(embed))
        await ctx.send(embed=embed)

    @commands.command(pass_context=True)
    async def add_owner(self, ctx, pseudo: str, nom: str):
        """!add_owner <pseudo> <nom> (permet d'ajouter le propriétaire d'un personnage')"""
        if pseudo in self.bot.players:
            self.bot.players[pseudo]['owner'] = nom
            self.bot.save()
            await ctx.send(f'{nom} ajouté en tant que propriétaire de {pseudo}')
        else:
            await ctx.send(f'{pseudo} n\'est pas dans la liste des joueurs.')

    @commands.command(pass_context=True)
    async def add_player(self, ctx, name: str):
        """!add <nom> (permet d'ajouter un joueur à la liste)"""
        self.bot.players[name] = {}
        self.bot.save()
        await ctx.send(f'{name} ajouté à la liste des joueurs')

    @commands.command(pass_context=True)
    async def del_player(self, ctx, name: str):
        """!del_player <nom> (permet de retirer un joueur de la liste)"""
        if name in self.bot.players:
            del self.bot.players[name]
            self.bot.save()
            await ctx.send(f'{name} retiré de la liste des joueurs')
        else:
            await ctx.send(f'{name} ne faisait pas partie de la liste des joueurs')

    @commands.command(pass_context=True)
    async def add_guild_url(self, ctx, guildURL):
        """!add_guild_url <url> (permet d'ajouter tous les joueurs de la guilde en lien')"""
        # get players and id from the guild URL
        self.bot.guild['url'] = guildURL
        thread = ThreadMeThat(ctx, players_from_guild, {
                              "url": guildURL}, self.call_back_guild_url)
        thread.start()
        await ctx.send("Ajout des joueurs de la guilde en cours ...")

    @commands.command(pass_context=True)
    async def update_jobs(self, ctx):
        # Update players jobs levels
        """!update_jobs (permet de mettre à jour les métiers des joueurs)"""
        await ctx.send("Mise à jour des métiers en cours...")
        thread = ThreadMeThat(ctx, update_all_players_jobs, {
                              "players": self.bot.players}, self.call_back_update)
        thread.start()

    # CALLBACKS ###############################################################
    def call_back_guild_url(self, newPlayers, ctx, sendMsg=True):
        added = ''
        updated = ''
        players = self.bot.players
        for name, data in newPlayers.items():
            if name in players:
                # Update player ID
                updated += f'\n\t{name}'
                players[name]['id'] = data['id']
            else:
                added += f"\n\t{name}"
                players[name] = {'id': data['id']}
        self.bot.save()

        # Cannot await a function in a thread so we cannot ctx.send messages
        if sendMsg:
            message = ''
            if added:
                message += 'Added following players: '
                message += f'{added}'
            if updated:
                message += '\nUpdated id of following players:'
                message += f'{updated}'
            self.message_not_sent.append({'ctx': ctx, 'message': message})

    def call_back_update(self, new_players_dict, ctx, sendMsg=True):

        self.bot.players = new_players_dict
        self.bot.save()
        if sendMsg:
            message = "Updated job data"
            self.message_not_sent.append({'ctx': ctx, 'message': message})


async def setup(bot):
    await bot.add_cog(Players(bot))


class ThreadMeThat(threading.Thread):

    def __init__(self, ctx, function, arguments, callback, sendMsg=None, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)

        self.function = function
        self.args = arguments
        self.ctx = ctx
        self.callback = callback
        self.msg = sendMsg

    def run(self):
        logging(f'Start function {self.function.__name__}')
        result = self.function(**self.args)
        logging(f'{self.function.__name__} done')

        self.callback(result, self.ctx, self.msg)
