import os
import json
import discord
from discord.ext import commands
from src.utils.logging import logging


class Dofutils(commands.Bot):
    def __init__(self):
        self.players = {}
        self.guild = {'url': None}
        self.token = None
        self.setup_env()
        self.load()
        default_intents = discord.Intents.default()
        # Define basic intents
        default_intents.message_content = True
        super().__init__(command_prefix="!", intents=default_intents)

    # Init methods ############################################################
    async def start(self, *args, **kwargs):
        if self.token is None:
            raise ("Token wrongly loaded")
        return await super().start(self.token)

    def setup_env(self):
        """
        Setup the environment files needed for the bot to work.
        """
        if not os.path.exists(r"data/"):
            os.mkdir(r"data/")

        if not os.path.exists(r"data/guild.json"):
            with open(r"data/guild.json", 'w+') as f:
                json.dump(self.guild, f, indent=2)

        if not os.path.exists(r"data/player.json"):
            with open(r"data/player.json", 'w+') as f:
                json.dump(self.players, f, indent=2)

        if not os.path.exists(r"data/token.json"):
            token = input("Give your bot token: ")
            with open(r"data/token.json", 'w+') as f:
                json.dump({"token": token}, f, indent=2)

    # Save methods ############################################################
    def save(self):
        """
        Start all saving processes
        """
        self.save_players()
        self.save_guild()

    def save_players(self):
        """
        Save player to a json file
        """
        with open(r"data/player.json", 'w+') as f:
            json.dump(self.players, f, indent=2)

    def save_guild(self):
        """
        Save guild to a json file
        """
        with open(r"data/guild.json", 'w+') as f:
            json.dump(self.guild, f, indent=2)

    # Load methods ############################################################
    def load(self):
        """
        Start all loading data processes
        """
        self.load_token()
        self.load_players()
        self.load_guild()

    def load_token(self):
        """
        Load token from the token.json file
        """
        with open(r"data/token.json", 'r') as f:
            self.token = json.load(f)['token']

    def load_players(self):
        """
        Load players data from the players.json file
        """
        with open(r"data/player.json", 'r+') as f:
            data = json.load(f)
            if data:
                self.players = data

    def load_guild(self):
        """
        Load players data from the guild.json file
        """
        with open(r"data/guild.json", 'r+') as f:
            data = json.load(f)
            if data:
                self.guild = data
