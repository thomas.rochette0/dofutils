import logging
from discord import Embed
from src.utils.logging import logging

def create_embed(titre, desc=None, field=None, values=None, inline=True):
    if desc:
        embed = Embed(title=titre, color=0xDFC845, description=desc)
    else:
        embed = Embed(title=titre, color=0xDFC845)
    logging('ici')
    if field and values and isinstance(field, list) and isinstance(values, list):
        dico = dict(zip(field, values))
        logging(f'là {dico}')
        for key, val in dico.items():
            embed.add_field(name=key, value=val, inline=inline)
    return embed
