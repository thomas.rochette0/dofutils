import sys
import datetime


def logging(msg):
    with open('app.log', 'a') as log:
        log.write(datetime.datetime.now().isoformat(sep=' ') + '  |  ' + str(msg) + '\n')
